FROM haskell:8.8.2 as builder

COPY . /app
WORKDIR /app

RUN cabal update && \
    cabal build all && \
    cabal test all
    
FROM scratch
COPY --from=builder /app/dist-newstyle/build/x86_64-linux/ghc-8.8.2/resistorBuilder-2.0.0/x/resistorBuilder/build/resistorBuilder/resistorBuilder /usr/local/bin/resistor-builder
ENTRYPOINT ["resistor-builder"]
